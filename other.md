# Other significant changes since 2.4.0

## Other

- RDoc performance improvement, new lexer.
- Upgraded regex engine, includes absent operator.
- Low-level File API changes.
- Lots of specs added! (as in RSpec)

# References

- https://blog.jetbrains.com/ruby/2017/10/10-new-features-in-ruby-2-5/
- https://www.ruby-lang.org/en/news/2017/10/10/ruby-2-5-0-preview1-released/
- https://github.com/ruby/ruby/blob/v2_5_0_preview1/NEWS
