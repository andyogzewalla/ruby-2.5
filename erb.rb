# ruby erb.rb

# frozen_string_literal: true

require 'erb'
require 'ostruct'

erb_template = <<~ERB
  Hello, <%= name %>! Welcome to <%= place %>!
ERB

# OLD way
ostruct = OpenStruct.new(name: "Ancient World", place: "a terrible place")
binds = ostruct.instance_eval { binding } # Eww!
puts ERB.new(erb_template).result(binds)

# NEW way
binds = { name: "New World", place: "paradise" }
puts ERB.new(erb_template).result_with_hash(binds)
