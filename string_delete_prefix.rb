# ruby string_delete_prefix.rb

s = "Hello, World!"
puts s

s.delete_prefix!("Hello, ")
puts s
# => "World!"

s.delete_suffix!("!")
puts s
# => "World"

#---------------------------------------------------------------------------------------------------

# require 'benchmark/ips'
# require 'active_support/core_ext/string/filters'
# Benchmark.ips do |x|
#   pattern = /\Aabc/;
#   prefix = "abc"
#
#   x.report("sub!") { "abcdef".sub!(pattern, '') }
#   x.report("gsub!") { "abcdef".gsub!(pattern, '') }
#   x.report("remove (based on gsub!)") { "abcdef".remove(pattern) }  # `remove` comes from ActiveSupport
#   x.report("delete_prefix!") { "abcdef".delete_prefix!(prefix) }
#
#   x.compare!
# end
